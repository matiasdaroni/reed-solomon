import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  K = 3;
  Q = 16;
  // A = [];
  Gx = [3, 1, 0, 3, 0];
  Xnk = [];
  Ux = [13, 9];
  // C = [];
  // archivo = null;
  // trama = [];
  elementosGalois = [8, 4, 2, 1, 12, 6, 3, 13, 10, 5, 14, 7, 15, 11, 9];

  constructor(private _httpClient: HttpClient) {
    console.log('orexs: ', (((1 ^ 2) ^ 4) ^ 5) ^ 4 );
    this.Xnk[4] = 0; // Formo el polinomio X^n-k = X^4
    let aux = this.multiplicaPolys(this.Xnk, this.Ux);
    console.log('Multi: ', aux);
    let division = this.divisionPolys(aux, this.Gx);
    console.log('division: ', division);
  //  this._httpClient.get('http://localhost:4201').subscribe(
  //    Response => {
  //      console.log('Respuesta', Response);
  //    },
  //    error => {
  //      console.log('Error', error);
  //    });

    // let resto = [];
    // let aux = [];
    // aux[4] = 0;
    // this.A = this.multiplicaPolys(aux, this.U);

    // resto = this.divisionPolys(this.A, this.B);
    // this.C = _.cloneDeep(this.U);

    // for (let i = 0; i < resto.length; i++) {
    //   this.C.push(resto[i]);
    // }
    // console.log('C(x): ', this.C);

    // console.log(this.getPalbraCodificada(this.C));

    console.log('Prueba magica: ', 1744 >>> 4);

  }

    // onChange(fileList: FileList): void {
    //   let file = fileList[0];
    //   let fileReader: FileReader = new FileReader();
    //   let self = this;
    //   fileReader.onloadend = function(x) {
    //     self.archivo = fileReader.result;
    //     console.log('Contenido Archivo: ', self.archivo);
    //     self.convierteTexto();
    //   };
    //   fileReader.readAsText(file);

    // }

    // convierteTexto() {
    //   for (let i = 0; i < this.archivo.length; i++) {
    //     this.trama.push(this.archivo[i].charCodeAt(0));
    //   }

    //   console.log('Trama: ', this.trama);
    //   let idx = 0;
    //   console.log('Bitwise read 109: ', bitwise.byte.read(this.trama[0]));
    //   if (idx % 2 === 0) {
    //     console.log('StackOverflow: ', this.trama[idx / 2] >>> 4);
    //   }
    //     console.log('StackOverflow: ', this.trama[idx / 2] & 0x0F);
    // }

    getPalbraCodificada(c: number[]) {
      let codificacion = [];
      for (let i = 0; i < c.length; i++) {
        if (c[i] === 0) {
          codificacion.push(0);
        } else {
          codificacion.push(this.elementosGalois[c[i]]);
        }
      }
      return codificacion;
    }

    multiplicaPolys(a: number[], b: number[]) {
      let producto: number[] = [];
        for (let i = 0; i < a.length; i++) {
          for (let j = 0; j < b.length; j++) {
            if (a[i] !== undefined && b[j] !== undefined) {
              // let aux = a[i] + b[j];
              // if (aux !== 0) {
                producto[i + j] = a[i] + b[j];
              // }
            }
          }
        }

        return producto;
    }

    coef(a: number[], b: number[]) {
      // Definir coeficiente para la división
      let coef: number[] = [];
      coef[a.length - b.length] = a[a.length - 1] + b[b.length - 1];
      return coef;
    }

    divisionPolys(a: number[], b: number[]) {
      let grado = a.length - 1; // Guardo el grado del polinomio a dividir para poder iterar.
      let cociente = []; // Cociente que multiplica por el divisor para obtener resto.
      let restos = [];
      let dividendo = a;
      let primeraVez = true;

      while (grado >= (b.length - 1)) {
        cociente = this.coef(dividendo, b);
        console.log('Cociente: ', cociente);

        let index = restos.push(this.multiplicaPolys(cociente, b)); // Guardo restos en arreglo de restos.
        restos[index - 1].splice(restos[index - 1].length - 1, 1); // Suprimo la potencia mas grande.
        console.log('Restos: ', restos);

        if (primeraVez) {
          dividendo.splice(dividendo.length - 1, 1);
          let empty = true;
          dividendo.map((e) => {if (!_.isUndefined(e)) { empty = false; }});

          if (!empty) {
            this.procesaArraysEnGalois(dividendo, restos[index - 1]);
            console.log('Restos procesados en primera vez: ', restos);
            index = restos.push(this.reemplazaElementosGalois(dividendo, restos[index - 1], index));
          }

          dividendo = restos[index - 1];
          grado = restos[index - 1].length - 1;
          primeraVez = false;

        } else {
          restos[index - 2].splice(restos[index - 2].length - 1, 1);

          // Si ya tengo restos para sumar Reemplazo por elementos de galois
          this.procesaArraysEnGalois(restos[index - 2], restos[index - 1]);
          console.log('Restos procesados: ', restos);

          index = restos.push(this.reemplazaElementosGalois(restos[index - 2], restos[index - 1], index));

          dividendo = restos[index - 1];
          grado = restos[index - 1].length - 1;
        }
      }

      console.log('Restos Finales: ', restos);
      return restos.pop();
    }

    procesaArraysEnGalois(a: number[], b: number[]) {
      for (let i = 0; i < a.length; i++) {
        if (a[i] > this.Q - 2) {
          a[i] = a[i] % (this.Q - 1);
        }
      }

      for (let i = 0; i < b.length; i++) {
        if (b[i] > this.Q - 2) {
          b[i] = b[i] % (this.Q - 1);
        }
      }

    }

    reemplazaElementosGalois(a: number[], b: number[], index) {
      let max = Math.max(a.length, b.length);
      let resultado = [];

      for (let i = max - 1; i >= 0; i--) {
        resultado[i] = this.orexRestos(a[i], b[i]);
      }

      console.log('Resultado: ', resultado);
      return resultado;
    }

    orexRestos(i: number, j: number) {
      if (i !== undefined) {
        return this.elementosGalois.indexOf(this.elementosGalois[i] ^ this.elementosGalois[j]);
      } else if (j !== undefined) {
        return this.elementosGalois.indexOf(this.elementosGalois[j]);
      }
    }
  }

